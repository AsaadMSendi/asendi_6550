
# importing Libraries
import sim
import time
import numpy as np
import cv2
import moveRobot
import vision
import weldingTorch

# connect with coppeliasim
sim.simxFinish(-1)
clientID = sim.simxStart('127.0.0.1', 19997, True, True, 5000, 5)

if clientID == -1:
    print('Failed to connect to CoppeliaSim remote API server')
    sim.simxFinish(clientID)

# Getting object handles
_, visionSence = sim.simxGetObjectHandle(clientID, 'vs1', sim.simx_opmode_oneshot_wait)              # For vision sensor
_, target = sim.simxGetObjectHandle(clientID, 'Target', sim.simx_opmode_blocking)               # For Target of roboarm
_, proximity = sim.simxGetObjectHandle(clientID, 'Proximity_sensor', sim.simx_opmode_blocking)  # For proximity sensor
_, welding = sim.simxGetObjectHandle(clientID, "WeldingTorchActiveTip", sim.simx_opmode_blocking)

# Move robot to initial position
firstPoint = (1.033, 0.072, 0.3155, 0, 0, 0)
ini_posi = firstPoint
moveRobot.move(clientID, target, ini_posi, 1)


# Getting the first image
err, resolution, image = sim.simxGetVisionSensorImage(clientID, visionSence, 0, sim.simx_opmode_streaming)

# make welding torch disbled
res = sim.simxSetObjectIntParameter(clientID, welding, sim.sim_objintparam_visibility_layer, 0, sim.simx_opmode_oneshot)

# Getting the images
if (sim.simxGetConnectionId(clientID) != -1):
        time.sleep(2)
        target_z = 0
        
        # Move the target down until the proximity sensor detects an object
        while True: 
            # Move the target down
            target_z += 0.01
            moveRobot.move(clientID, target, (ini_posi[0], ini_posi[1], ini_posi[2]-target_z, 0, 0, 0), 1)
            
            # Check if the proximity sensor detects an object
            _, detection_state, _, _, _ = sim.simxReadProximitySensor(clientID, proximity, sim.simx_opmode_blocking)

            if detection_state:
                print("Object detected!")
                break
        
            
        # Update the initial position
        for i in range(100):
            ini_posi = sim.simxGetObjectPosition(clientID, target, -1, sim.simx_opmode_streaming)[1]
        print(ini_posi)

        firstWeldPoint = []

        #### IF WELDINGSEAM IS VERTICAL ######
        target_y = 0    
        while True:
            # Move the target through y axis till the welding seam found
            target_y += 0.01
            moveRobot.move(clientID, target, (ini_posi[0], ini_posi[1]+target_y, ini_posi[2], 0, 0, 0), 1)

            # checking the welding seam
            gray = vision.lookInGray(clientID, visionSence)
            grayPresentageVal = vision.get_gray_image_pixel_percentages(gray)

            if grayPresentageVal > 23.5:
                firstWeldPoint = [ini_posi[0], ini_posi[1]+target_y, ini_posi[2], 0, 0, 0]
                break
        
        # Weld the point
        weldingTorch.weld(clientID, target)

        # Update the initial position
        for i in range(100):
            ini_posi = sim.simxGetObjectPosition(clientID, target, -1, sim.simx_opmode_streaming)[1]

        target_x = 0
        while True:
            # Move and weld while the welding seam ends
            target_x += 0.1
            moveRobot.move(clientID, target, (ini_posi[0]+target_x, ini_posi[1], ini_posi[2], 0, 0, 0), 1)

            # If the end of the welding seam detected, stop the move
            # checking the welding seam
            gray = vision.lookInGray(clientID, visionSence)
            blackPresentageVal = vision.get_black_image_pixel_percentages(gray)

            if blackPresentageVal > 25.5:
                # Move to the first welded position
                moveRobot.move(clientID, target, firstWeldPoint, 1)
                break

            # weld
            weldingTorch.weld(clientID, target)
        
        # Go other side of welding seam
        target_x = 0
        while True:
            # Move and weld while the welding seam ends
            target_x += 0.1
            moveRobot.move(clientID, target, (ini_posi[0]-target_x, ini_posi[1], ini_posi[2], 0, 0, 0), 1)

            # If the end of the welding seam detected, stop the move
            # checking the welding seam
            gray = vision.lookInGray(clientID, visionSence)
            blackPresentageVal = vision.get_black_image_pixel_percentages(gray)

            if blackPresentageVal > 25.5:
                # Move to the first welded position
                moveRobot.move(clientID, target, firstWeldPoint, 1)
                break

            # weld
            weldingTorch.weld(clientID, target)

            


        

        


