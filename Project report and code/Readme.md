Instructions to run the simulation.
Download the Robotic_Welder Zip folder and extract it to a convenient location on your local drive.
Open SPYDER through Anaconda Navigator.
Make sure the required dependencies are there; code will not without OPENCV
Set the current directory of SPYDER to where the extracted Robotic_Welder folder is kept. All files inside the Robotic_Welder folder will appear inside the Spyder GUI.
Open the Python Script files _simpleTest.py_ and _welder1.py_ both of these scripts will open in seperate tabs in the GUI.
Now open the CoppeliaSim File _Welder.ttt_ from the main Robotic_Welder folder.
Run the simulation.
Now go back to SPYDER and run the script simpleTest.py. If it runs successfully, means that a connection has been established between CoppeliaSim and Python. 
Now go to the main welder1.py script that is open in the other tab. Run the script.
Go back to CoppeliaSim to see the simulation.
