
import numpy as np
from scipy.special import gammainc

import math
import random


    
def sample_spherical2(npoints, ndim=3):   
    
    radius = 0.29
    x = np.random.normal(0,1,(npoints,ndim)) 

    z = np.linalg.norm(x, axis=1) 
    z = z.reshape(-1,1).repeat(x.shape[1], axis=1) 

    Points = x/z * radius * np.sqrt(ndim) 
    return Points
  
def boundingbox(objHandle):
    xMin=sim.getObjectFloatParam(objHandle,sim.objfloatparam_objbbox_min_x)
    yMin=sim.getObjectFloatParam(objHandle,sim.objfloatparam_objbbox_min_y)
    zMin=sim.getObjectFloatParam(objHandle,sim.objfloatparam_objbbox_min_z)
    
    xMax=sim.getObjectFloatParam(objHandle,sim.objfloatparam_objbbox_max_x)
    yMax=sim.getObjectFloatParam(objHandle,sim.objfloatparam_objbbox_max_y)
    zMax=sim.getObjectFloatParam(objHandle,sim.objfloatparam_objbbox_max_z)
    p1=sim.getObjectPosition(objHandle,sim.handle_world) 
    
    return xMin, yMin,xMax,yMax
    
def sysCall_thread():
     
    #x, y, z = sample_spherical(150)
    Points   = sample_spherical2(150)     
    sphere=sim.createPrimitiveShape(sim.primitiveshape_spheroid,[1,1,1])
    spherepostion=sim.getObjectPosition(sphere,-1)
    spherepostion[2]=spherepostion[2]+1
    sim.setObjectPosition(sphere,-1,spherepostion)
    print(boundingbox(sphere) )
    
    
    #for i in range(x.shape[0]):    
    for p in Points:
        cone=sim.createPrimitiveShape(sim.primitiveshape_cone,[0.02,0.02,0.02])
        
        u1=np.random.rand()
        u2=np.random.rand()
        u3=np.random.rand()
        a=np.sqrt(1-u1) * np.sin(2*np.pi*u2)  
        b=np.sqrt(1-u1) * np.cos(2*np.pi*u2)  
        c=np.sqrt(u1) * np.sin(2*np.pi*u3)  
        d= np.sqrt(u1) * np.cos(2*np.pi*u3) 

        sim.setObjectQuaternion(cone,-1,[a,b,c,d])
        coneposition = sim.getObjectPosition(cone,-1)
        #print(cone) 
        #coneposition[0]=x[i]
        #coneposition[1]=y[i]
        #coneposition[2]=z[i]
        coneposition[0]=p[0]
        coneposition[1]=p[1]
        coneposition[2]=p[2]
        #print(coneposition)
        sim.setObjectPosition(cone,sphere,coneposition)
    pass

