
D* grid planning

"""
import math

from sys import maxsize

import matplotlib.pyplot as plt

import numpy as np
show_animation = False


class State:

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.parent = None
        self.state = "."
        self.t = "new"  # tag for state
        self.h = 0
        self.k = 0

    def cost(self, state):
        if self.state == "#" or state.state == "#":
            return maxsize

        return math.sqrt(math.pow((self.x - state.x), 2) +
                         math.pow((self.y - state.y), 2))

    def set_state(self, state):
        """
        .: new
        #: obstacle
        e: oparent of current state
        *: closed state
        s: current state
        """
        if state not in ["s", ".", "#", "e", "*"]:
            return
        self.state = state


class Map:

    def __init__(self, row, col):
        self.row = row
        self.col = col
        self.map = self.init_map()

    def init_map(self):
        map_list = []
        for i in range(self.row):
            tmp = []
            for j in range(self.col):
                tmp.append(State(i, j))
            map_list.append(tmp)
        return map_list

    def get_neighbors(self, state):
        state_list = []
        for i in [-1, 0, 1]:
            for j in [-1, 0, 1]:
                if i == 0 and j == 0:
                    continue
                if state.x + i < 0 or state.x + i >= self.row:
                    continue
                if state.y + j < 0 or state.y + j >= self.col:
                    continue
                state_list.append(self.map[state.x + i][state.y + j])
        return state_list

    def set_obstacle(self, point_list):
        for x, y in point_list:
            if x < 0 or x >= self.row or y < 0 or y >= self.col:
                continue

            self.map[x][y].set_state("#")


class Dstar:
    def __init__(self, maps):
        self.map = maps
        self.open_list = set()

    def process_state(self):
        x = self.min_state()

        if x is None:
            return -1

        k_old = self.get_kmin()
        self.remove(x)

        if k_old < x.h:
            for y in self.map.get_neighbors(x):
                if y.h <= k_old and x.h > y.h + x.cost(y):
                    x.parent = y
                    x.h = y.h + x.cost(y)
        elif k_old == x.h:
            for y in self.map.get_neighbors(x):
                if y.t == "new" or y.parent == x and y.h != x.h + x.cost(y) \
                        or y.parent != x and y.h > x.h + x.cost(y):
                    y.parent = x
                    self.insert(y, x.h + x.cost(y))
        else:
            for y in self.map.get_neighbors(x):
                if y.t == "new" or y.parent == x and y.h != x.h + x.cost(y):
                    y.parent = x
                    self.insert(y, x.h + x.cost(y))
                else:
                    if y.parent != x and y.h > x.h + x.cost(y):
                        self.insert(y, x.h)
                    else:
                        if y.parent != x and x.h > y.h + x.cost(y) \
                                and y.t == "close" and y.h > k_old:
                            self.insert(y, y.h)
        return self.get_kmin()

    def min_state(self):
        if not self.open_list:
            return None
        min_state = min(self.open_list, key=lambda x: x.k)
        return min_state

    def get_kmin(self):
        if not self.open_list:
            return -1
        k_min = min([x.k for x in self.open_list])
        return k_min

    def insert(self, state, h_new):
        if state.t == "new":
            state.k = h_new
        elif state.t == "open":
            state.k = min(state.k, h_new)
        elif state.t == "close":
            state.k = min(state.h, h_new)
        state.h = h_new
        state.t = "open"
        self.open_list.add(state)

    def remove(self, state):
        if state.t == "open":
            state.t = "close"
        self.open_list.remove(state)

    def modify_cost(self, x):
        if x.t == "close":
            self.insert(x, x.parent.h + x.cost(x.parent))

    def run(self, start, end):

        rx = []
        ry = []

        self.insert(end, 0.0)

        while True:
            self.process_state()
            if start.t == "close":
                break

        start.set_state("s")
        s = start
        s = s.parent
        s.set_state("e")
        tmp = start

        while tmp != end:
            tmp.set_state("*")
            rx.append(tmp.x)
            ry.append(tmp.y)
            # print(rx,ry)
            # if show_animation:
            #     plt.plot(rx, ry, "-r")
            #     plt.pause(0.01)
            #     plt.cla()
            if tmp.parent.state == "#":
                self.modify(tmp)
                continue
            tmp = tmp.parent
        tmp.set_state("e")

        return rx, ry

    def modify(self, state):
        self.modify_cost(state)
        while True:
            k_min = self.process_state()
            if k_min >= state.h:
                break


# Function to convert 2D continuous points into grid index
# https://l.messenger.com/l.php?u=https%3A%2F%2Fstackoverflow.com%2Fquestions%2F62778939%2Fpython-fastest-way-to-map-continuous-coordinates-to-discrete-grid&h=AT3RNZlXWmIYQdFxWBEd_LliduGSA8msDnnXwrn8llyq4SvGjMR9gSu1mXzkTM5P2RaDXSvmviQhC3Xtw3g6fNfDHUVTY2sw0hKnfmEspYI--QKdRDbOHG8u2ZGXS_kRPw4


def pointtogrid(x,y,grid_offset,grid_spacing):
  point = np.array([x,y])
  gridindex = np.round((point - grid_offset) / grid_spacing)
  return gridindex+50

# Function to convert grid index into 2D continuous points.
def gridtopoint(gridindex,grid_offset,grid_spacing):
  p=grid_offset + gridindex * grid_spacing
  return  p

# Run D* on discrete grid using D* algorithm
def findbestpath(setofobs,newstart1,end):
    m = Map(100, 100)
    ox, oy = [], []
    # Adding boundary wall
    for i in range(0, 100):
        ox.append(i)
        oy.append(0)
    for i in range(0, 100):
        ox.append(100)
        oy.append(i)
    for i in range(0, 101):
        ox.append(i)
        oy.append(100)
    for i in range(0, 101):
        ox.append(0)
        oy.append(i)
        
    # Define grid offset and spacing.
    grid_offset = np.array([0, 0])
    grid_spacing = np.array([.05, .05])
    
    # safedistance
    safedistance=5
    
    # Convert boundary of set of obstacles (Two wall) into grid indices
    for so in setofobs:
        x1,y1=pointtogrid((-1)*so[0],(-1)*so[1],grid_offset,grid_spacing)
        x2,y2=pointtogrid((-1)*so[2],(-1)*so[3],grid_offset,grid_spacing)

        # Append the walls in the obstacles list.  
        for x in range(int(x2-safedistance),int(x1+safedistance)):
            for y in range(int(y2-safedistance),int(y1+safedistance)) :
                ox.append(int(x))
                oy.append(int(y))
          
    m.set_obstacle([(i, j) for i, j in zip(ox, oy)])
    
    #Convert start and end cuboid coordinates into grid indices
    x1,y1=pointtogrid((-1)*newstart1[0],(-1)*newstart1[1],grid_offset,grid_spacing)
    x2,y2=pointtogrid((-1)*end[0],(-1)*end[1],grid_offset,grid_spacing)
    start = [int(x1),int(y1)]
    goal = [int(x2),int(y2)]
    

    # Star running the D* algorithm to find the optimal path.
    start = m.map[start[0]][start[1]]
    end = m.map[goal[0]][goal[1]]
    dstar = Dstar(m)
    rx, ry = dstar.run(start, end)

    # Add the optimal list of points into list and return to the caller function.
    res=[]
    for x,y in zip(rx,ry): 
        res.append(gridtopoint([x,y],grid_offset,grid_spacing))
    return res

def boundingbox(objHandle):
    xMin=sim.getObjectFloatParam(objHandle,sim.objfloatparam_objbbox_min_x)
    yMin=sim.getObjectFloatParam(objHandle,sim.objfloatparam_objbbox_min_y)
    zMin=sim.getObjectFloatParam(objHandle,sim.objfloatparam_objbbox_min_z)
    
    xMax=sim.getObjectFloatParam(objHandle,sim.objfloatparam_objbbox_max_x)
    yMax=sim.getObjectFloatParam(objHandle,sim.objfloatparam_objbbox_max_y)
    zMax=sim.getObjectFloatParam(objHandle,sim.objfloatparam_objbbox_max_z)
    p1=sim.getObjectPosition(objHandle,sim.handle_world) 
    
    return p1[0]+xMin, p1[1]+yMin,p1[0]+xMax,p1[1]+yMax

    
def sysCall_thread():
    #e.g. non-synchronized loop: 
 
    # Enable auto thread switching
    sim.setThreadAutomaticSwitch(True)
  
    # Find the bounding box for the first wall (orange)
    objHandle= sim.getObject("/Wall1")
    xMin1,yMin1, xMax1,yMax1=  boundingbox(objHandle)  
    
    # Find the bounding box for the second wall (brown)
       
    objHandle= sim.getObject("/Wall")
    xMin2,yMin2, xMax2,yMax2=  boundingbox(objHandle)
   
    # Create a set of objects
    
    setofobs= [np.array([xMin1,yMin1, xMax1,yMax1]),np.array([xMin2,yMin2, xMax2,yMax2]) ]
   
    #get the position for start cuboid
    objHandle= sim.getObject("/Start")

    start=sim.getObjectPosition(objHandle,-1)
    
    #get the position for Goal cuboid
    objHandle= sim.getObject("/Goal")

    end=sim.getObjectPosition(objHandle,-1) 
       
    # find the best path using D* Algorithm
    res=findbestpath(setofobs,start,end)
    
    # Draw the path and move the target of quadcopter based on the path
    objHandle= sim.getObject("/target")
    px=res[0][0]
    py=res[0][1]    
    for i in range(len(res)):
        p=res[i]   
        offset=2.5
        lineThinkness=2
        color=[2,2,2]    
        c=sim.addDrawingObject(sim.drawing_lines,lineThinkness,0,-1,99999,color)
        lineData=[px*(-1)+offset,py*(-1)+offset,start[2],p[0]*(-1)+offset,p[1]*(-1)+offset,start[2]]
        sim.addDrawingObjectItem(c,lineData)
        px=p[0]
        py=p[1]    
    
        # Get the current postion of the target.
        p3=sim.getObjectPosition(objHandle,-1)

        # Change the postionf of the target to new point.
        p3[0]=px*(-1)+offset
        p3[1]=py*(-1)+offset    
        sim.setObjectPosition(objHandle,-1,p3)
        
        # Pause simulation for 3 seconds.
        haltUntilSimulationTime=sim.getSimulationTime()+3 

    pass
