#python
import random
def sysCall_thread():
    #e.g. non-synchronized loop:
        sim.setThreadAutomaticSwitch(True)
        objHandle = sim.getObject("/target")
        while True:
        Width=5
        Hight=6

            p=sim.getObjectPosition(objHandle,-1)
            if (p[0]>-2 or p[0]<2):
                p[0]=p[0]+random.choice([0.1,-0.1])
            
            if (p[1]>-2 or p[1]<2):
                p[1]=p[1]+random.choice([0.1,-0.1])
                
            sim.setObjectPosition(objHandle,-1,p)
        pass
        



1-I created a virtual environment in CoppeliaSim and added a quadcopter model to it.
I defined a boundary within which the quadcopter should move randomly and move along the X and Y axes within a designated display area.
I implemented an algorithm that randomly selects a direction and a distance for the quadcopter to move within the boundary and randomly moving in an unpredictable manner.
I implemented a detection algorithm that uses sensors on the quadcopter to detect any intruders within its surroundings. When an intruder is detected, the quadcopter can either avoid the intruder or take a picture or video for further analysis.
To achieve these objectives, the algorithm sets the starting position (p[0] on the X-axis and p[1] on the Y-axis) within the specified area, and randomly selects movement increments.

