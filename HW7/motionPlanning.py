#python
import numpy as np

def sysCall_thread():
    dt = sim.getSimulationTimeStep()*20
    sim.setThreadAutomaticSwitch(False)
    p1 = sim.getObject('/C0')
    #print(p1)
    p2 = sim.getObject('/C1')
    #print(p2)
    data = sim.getStringSignal('pathValues')
    path1D_x = sim.unpackDoubleTable(data)
    #print(path1D_x)
    data1 = sim.getStringSignal('pathValues1')
    path1D_y = sim.unpackDoubleTable(data1)
    #print(path1D_y)
    data2 = sim.getStringSignal('pathValues2')
    path1D_z = sim.unpackDoubleTable(data2)
    #print(path1D_z)
    p3 = sim.getObject('/P0')
    p4 = sim.getObject('/P1')
    
    
    while True:
        count = 0
        count1 = len(path1D_x) - 1
        distant1 = np.linalg.norm(sim.getObjectPosition(p3, p1))
        #print(distant1)
        distant2 = np.linalg.norm(sim.getObjectPosition(p4, p2))
        #print(distant2)
        while (count < len(path1D_x)) and (count1 > 0):
            if ((distant1 < 0.7) and (distant2 < 0.7)):
                sim.setObjectPosition(p1, -1,[path1D_x[count],path1D_y[count],path1D_z[count]])
                sim.setObjectPosition(p2, -1,[path1D_x[count1],path1D_y[count1],path1D_z[count1]])
                count += 1
                count1 -= 1
                sim.wait(dt)
            else:
                #print('wait')
                
                sim.wait(10)

        

# See the user manual or the available code snippets for additional callback functions and details
