#python
import numpy as np
import math
from sys import maxsize

class CoppeliaSim_Manfold:
    def __init__(self, grid_res, bottom_left_corner, top_right_corner):
        
        self.origin = np.array(bottom_left_corner)
        self.top_right_corner = np.array(top_right_corner)
        self.grid_res = grid_res
    
    def transform (self, X):
        X_std = (X - X.min(axis=0)) / (X.max(axis=0) - X.min(axis=0))
        X_scaled = (X_std) * (self.top_right_corner - self.origin) + self.origin
        return X_scaled
        
    def populate_objects(self, obstacles):
        
        primitiveType = sim.primitiveshape_cuboid
        OBSTACLES_HEIGHT = 1
        sizes = [self.grid_res ,self.grid_res , OBSTACLES_HEIGHT]
        default_z = self.grid_res 
        
        shapeHandles = []
        for obs in obstacles:
            shapeHandle = sim.createPrimitiveShape(primitiveType, sizes)
            sim.setObjectPosition(shapeHandle, -1 , [obs[0], obs[1], default_z])
            shapeHandles.append(shapeHandle)
            
        groupHanle = sim.groupShapes(shapeHandles,True)
        
        sim.setObjectAlias(groupHanle,'obstacles')
    
    def transformPath(self, path):
        X_std = (path - path.min(axis=0)) / (path.max(axis=0) - path.min(axis=0))
        transfer_path = (X_std) * (self.top_right_corner - self.origin) + self.origin
        return transfer_path


class State:

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.parent = None
        self.state = "."
        self.t = "new"  # tag for state
        self.h = 0
        self.k = 0

    def cost(self, state):
        if self.state == "#" or state.state == "#":
            return maxsize

        return math.sqrt(math.pow((self.x - state.x), 2) +
                         math.pow((self.y - state.y), 2))

    def set_state(self, state):
        """
        .: new
        #: obstacle
        e: oparent of current state
        *: closed state
        s: current state
        """
        if state not in ["s", ".", "#", "e", "*"]:
            return
        self.state = state

class Map:

    def __init__(self, row, col):
        self.row = row
        self.col = col
        self.map = self.init_map()

    def init_map(self):
        map_list = []
        for i in range(self.row):
            tmp = []
            for j in range(self.col):
                tmp.append(State(i, j))
            map_list.append(tmp)
        return map_list

    def get_neighbors(self, state):
        state_list = []
        for i in [-1, 0, 1]:
            for j in [-1, 0, 1]:
                if i == 0 and j == 0:
                    continue
                if state.x + i < 0 or state.x + i >= self.row:
                    continue
                if state.y + j < 0 or state.y + j >= self.col:
                    continue
                state_list.append(self.map[state.x + i][state.y + j])
        return state_list

    def set_obstacle(self, point_list):
        for x, y in point_list:
            if x < 0 or x >= self.row or y < 0 or y >= self.col:
                continue

            self.map[x][y].set_state("#")

class Dstar:
    def __init__(self, maps):
        self.map = maps
        self.open_list = set()

    def process_state(self):
        x = self.min_state()

        if x is None:
            return -1

        k_old = self.get_kmin()
        self.remove(x)

        if k_old < x.h:
            for y in self.map.get_neighbors(x):
                if y.h <= k_old and x.h > y.h + x.cost(y):
                    x.parent = y
                    x.h = y.h + x.cost(y)
        elif k_old == x.h:
            for y in self.map.get_neighbors(x):
                if y.t == "new" or y.parent == x and y.h != x.h + x.cost(y) \
                        or y.parent != x and y.h > x.h + x.cost(y):
                    y.parent = x
                    self.insert(y, x.h + x.cost(y))
        else:
            for y in self.map.get_neighbors(x):
                if y.t == "new" or y.parent == x and y.h != x.h + x.cost(y):
                    y.parent = x
                    self.insert(y, x.h + x.cost(y))
                else:
                    if y.parent != x and y.h > x.h + x.cost(y):
                        self.insert(y, x.h)
                    else:
                        if y.parent != x and x.h > y.h + x.cost(y) \
                                and y.t == "close" and y.h > k_old:
                            self.insert(y, y.h)
        return self.get_kmin()

    def min_state(self):
        if not self.open_list:
            return None
        min_state = min(self.open_list, key=lambda x: x.k)
        return min_state

    def get_kmin(self):
        if not self.open_list:
            return -1
        k_min = min([x.k for x in self.open_list])
        return k_min

    def insert(self, state, h_new):
        if state.t == "new":
            state.k = h_new
        elif state.t == "open":
            state.k = min(state.k, h_new)
        elif state.t == "close":
            state.k = min(state.h, h_new)
        state.h = h_new
        state.t = "open"
        self.open_list.add(state)

    def remove(self, state):
        if state.t == "open":
            state.t = "close"
        self.open_list.remove(state)

    def modify_cost(self, x):
        if x.t == "close":
            self.insert(x, x.parent.h + x.cost(x.parent))

    def run(self, start, end):

        rx = []
        ry = []

        self.insert(end, 0.0)

        while True:
            self.process_state()
            if start.t == "close":
                break

        start.set_state("s")
        s = start
        s = s.parent
        s.set_state("e")
        tmp = start

        while tmp != end:
            tmp.set_state("*")
            rx.append(tmp.x)
            ry.append(tmp.y)
            if tmp.parent.state == "#":
                self.modify(tmp)
                continue
            tmp = tmp.parent
        tmp.set_state("e")

        return rx, ry

    def modify(self, state):
        self.modify_cost(state)
        while True:
            k_min = self.process_state()
            if k_min >= state.h:
                break

def getPath(manifold, startPoint, endPoint):
    
    GRID_RESOLUTION = 100
    DILATION_VALUE = 3
    
    m = DilateMap(GRID_RESOLUTION,GRID_RESOLUTION,DILATION_VALUE)
    
    m.set_obstacle(get_obstacles())

    
    #convert to planning domain coordinate
    start = manifold.getPlannerCoord(startPoint)
    goal = manifold.getPlannerCoord(endPoint)
 
    
    start = m.map[start[0]][start[1]]
    end = m.map[goal[0]][goal[1]]
    
    #compute shortest path using Dstar algorithm
    dstar = Dstar(m)
    rx,ry = dstar.run(start,end)
    
    #append goal location
    rx.append(goal[0])
    ry.append(goal[1])
    
    #convert path to copplia coordinate
    path = np.column_stack((rx,ry))
    pathCoppelia = manifold.transformPath(path)
    pathCoppelia[0, :] = startPoint
    
    return pathCoppelia

def get_obstacles(offset):
    m = Map(100,100)
    ox, oy = [], []
    for i in range(-10, 60):
        ox.append(i)
        oy.append(-10)
    for i in range(-10, 60):
        ox.append(60)
        oy.append(i)
    for i in range(-10, 61):
        ox.append(i)
        oy.append(60)
    for i in range(-10, 61):
        ox.append(-10)
        oy.append(i)
    for i in range(-10, 40):
        ox.append(20)
        oy.append(i)
    for i in range(0, 40):
        ox.append(40)
        oy.append(60 - i)
        
    m.set_obstacle([(i, j) for i, j in zip(ox, oy)])
    
    #add safety offset
    if(offset > 0):
        m.set_obstacle([(i + offset, j) for i, j in zip(ox, oy)])
        m.set_obstacle([(i - offset, j) for i, j in zip(ox, oy)])
        m.set_obstacle([(i, j + offset) for i, j in zip(ox, oy)])
        m.set_obstacle([(i, j - offset) for i, j in zip(ox, oy)])
    return m, ox, oy
    
def set_obstacles(obstacles):
    primitiveType = sim.primitiveshape_cuboid
    sizes = [0.25 ,0.25 , 1]
    shapeHandles = []
    for obs in obstacles:
        shapeHandle = sim.createPrimitiveShape(primitiveType, sizes)
        sim.setObjectPosition(shapeHandle, -1 , list(obs))
        shapeHandles.append(shapeHandle)
            
    groupHanle = sim.groupShapes(shapeHandles,True)
        
    sim.setObjectAlias(groupHanle,'obstacles')


def getOjbectPosition(objName):
    handle = sim.getObject(objName)
    return sim.getObjectPosition(handle,-1)
    
def drawPath(path):
    lineSize = 6
    maximumLines = 99999
    red = [1,0,0]
    
    drawingObjectHandle = sim.addDrawingObject(sim.drawing_lines,lineSize,0.0,-1,maximumLines,red)
    
    for curr,pre in zip(path[1:],path[:-1]):
        line = np.append(pre,curr)
        line = list(line)
            
        sim.addDrawingObjectItem(drawingObjectHandle,line)
        
def min_max_scale(data, local_min,local_max, global_min, global_max):
    X_std = (data - local_min)/(local_max - local_min)
    x_scaled = (X_std) * (global_max - global_min) + global_min
    return x_scaled
    
def createTraj(path):
    
    def get_quaternion_from_euler(roll, pitch, yaw):
      """
      Convert an Euler angle to a quaternion.
       
      Input
        :param roll: The roll (rotation around x-axis) angle in radians.
        :param pitch: The pitch (rotation around y-axis) angle in radians.
        :param yaw: The yaw (rotation around z-axis) angle in radians.
     
      Output
        :return qx, qy, qz, qw: The orientation in quaternion [x,y,z,w] format
      """
      qx = np.sin(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) - np.cos(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)
      qy = np.cos(roll/2) * np.sin(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.cos(pitch/2) * np.sin(yaw/2)
      qz = np.cos(roll/2) * np.cos(pitch/2) * np.sin(yaw/2) - np.sin(roll/2) * np.sin(pitch/2) * np.cos(yaw/2)
      qw = np.cos(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)
     
      return [qx, qy, qz, qw]
    
    ctrlPts = []
    vxmin = vymin =vzmin = 0.0
    vxmax = vymax = vzmax = 4.0
    ppath = []
    minMaxVel = minMaxAccel = []
    for p in path:
        yaw = math.atan2(p[1],p[0])
        pose = [p[0],p[1], 1]
        ori = get_quaternion_from_euler(0, 0, 0)
        ppath.extend(pose.copy())
        pose.extend(ori)
        ctrlPts.extend(pose)
        
        v = [vxmin, vxmax, vymin, vymax, vzmin, vzmax]
        minMaxVel.extend(v)
        
    pathLengths, totalLength = sim.getPathLengths(ctrlPts,7)
    dt = sim.getSimulationTimeStep()
    #print(totalLength)
    pathHandle = sim.createPath(ctrlPts, 0, totalLength // dt)
    #print(pathHandle)
    #data = sim.packDoubleTable(s_path.tolist())
    #sim.setStringSignal('pathValues', data)
    sim.setInt32Signal('pathHandle',pathHandle)
    
def createTraj_1(path):
    data = sim.packDoubleTable(path.tolist())
    sim.setStringSignal('pathValues', data)
    

def sysCall_init():
    
    #safety offset 
    offset = 8
    
    #world
    floor_min = -2.5
    floor_max = 2.5
    
    obs_min = -10
    obs_max = 60
    
    start = np.array(getOjbectPosition('/P0'))
    #print(start)
    goal = np.array(getOjbectPosition('/P1'))
    #print(goal)  

    
    #scale the startpoint and endpoint
    start = min_max_scale(start[:-1],floor_min,floor_max,obs_min,obs_max).astype(int)
    #print(start)
    goal = np.ceil(min_max_scale(goal[:-1],floor_min,floor_max,obs_min,obs_max)).astype(int)
    #print(goal)
    
    # get obstacle map
    m, ox, oy = get_obstacles(offset)    
    
    #scale the map to world
    s_ox = min_max_scale(np.array(ox),obs_min,obs_max,floor_min,floor_max)
    #print(s_ox)
    x_oy = min_max_scale(np.array(oy),obs_min,obs_max,floor_min,floor_max)
    #print(s_ox)
    
    #set obstacle in world
    s_oz = np.full(s_ox.shape[0],0.1)
    s_obs = np.stack([s_ox,x_oy,s_oz],axis=1)

    #set_obstacles(s_obs)
    
    
    #run D*
    start = m.map[start[0]][start[1]]
    end = m.map[goal[0]][goal[1]]
    dstar = Dstar(m)
    rx, ry = dstar.run(start, end)

    
    #map path to world
    s_rx = min_max_scale(np.array(rx),obs_min,obs_max,floor_min,floor_max)
    s_ry = min_max_scale(np.array(ry),obs_min,obs_max,floor_min,floor_max)
    
    s_rz = np.full(s_rx.shape[0],0.05)
    
    s_path = np.stack([s_rx,s_ry,s_rz],axis=1)
    
    #print(type(s_path))
    drawPath(s_path)
    
    #create a path handle
    #createTraj(s_path)
    
    #sending path
    #print(s_path.tolist())
    
    #data = sim.packDoubleTable(s_path.tolist())
    data = sim.packDoubleTable(s_rx.tolist())
    sim.setStringSignal('pathValues', data)
    
    data1 = sim.packDoubleTable(s_ry.tolist())
    sim.setStringSignal('pathValues1', data1)
    
    data2 = sim.packDoubleTable(s_rz.tolist())
    sim.setStringSignal('pathValues2', data2)
    
    
    #create 2 goal
    #goal_1 = sim.createPrimitiveShape(sim.primitiveshape_cuboid,[0.2,0.2,0.2])
    #sim.setObjectPosition(goal_1, -1, getOjbectPosition('/P0'))
    #goal_2 = sim.createPrimitiveShape(sim.primitiveshape_cuboid,[0.2,0.2,0.2])
    #sim.setObjectPosition(goal_2, -1, getOjbectPosition('/P1'))
    
    
        
    
    #pass

def sysCall_actuation():
    # put your actuation code here
   
    pass

def sysCall_sensing():
    # put your sensing code here
    pass

def sysCall_cleanup():
    # do some clean-up here
    pass

# See the user manual or the available code snippets for additional callback functions and details
