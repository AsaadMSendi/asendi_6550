#python
import math
import numpy as np

L = 0.381
R = 0.195 / 2

def uni_to_diff(v,w):
    vR = (2 * v + w * L) / (2 * R)
    vL = (2 * v - w * L) / (2 * R)
    return vR, vL
    
def sysCall_thread():
    # e.g. non-synchronized loop:
    sim.setThreadAutomaticSwitch(False)
    motorLeft =  sim.getObject('./leftMotor')
    motorRight = sim.getObject('./rightMotor')
    goal = sim.getObject('/C1')
    robot = sim.getObject('.')
    
    
    obstacles = sim.createCollection(0)
    sim.addItemToCollection(obstacles,sim.handle_all,-1,0)
    sim.addItemToCollection(obstacles,sim.handle_tree,robot,1)
    
    usensors={}
    i = 1
    while i <= 16:

        usensors[i]=sim.getObject("./ultrasonicSensor",{'index':(i-1)})
        sim.setObjectInt32Param(usensors[i],sim.proxintparam_entity_to_detect,obstacles)
        i += 1
        
    noDetectionDist = 0.5
    maxDetectionDist = 0.2
    detect=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    braitenbergL=[-0.2,-0.4,-0.6,-0.8,-1,-1.2,-1.4,-1.6, 0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
    braitenbergR=[-1.6,-1.4,-1.2,-1,-0.8,-0.6,-0.4,-0.2, 0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
    
    
    sim.setObjectParent(goal,-1,True)
    Kp = .25
    Kalpha = .35
    
    while True:
        GTR = np.array(sim.getObjectPosition(goal, robot))
        
        i = 1
        while i < 16:
            res, dist, detectedPoint, detectedObjectHandle, detectedSurfaceNormalVector = sim.readProximitySensor(usensors[i])
            print(res)
            print(dist)
            if (res>0) and (dist<noDetectionDist):
                if (dist<maxDetectionDist):
                    dist=maxDetectionDist
                detect[i]=1-((dist-maxDetectionDist)/(noDetectionDist-maxDetectionDist))
            else:
                detect[i]=0
            i+=1
        
        
        # position error
        err = np.linalg.norm(GTR)
        
        #accept goal region
        if err < L:
            err = 0
        
        vR, vL = 0 , 0
        
        if err > 0:
            vR, vL = uni_to_diff(Kp * GTR[0], Kalpha * GTR[1])
        
        while i < 16:
            vL = vL + braitenbergL[i]*detect[i]
            vR = vR + braitenbergR[i]*detect[i]
            i+=1
        
        sim.wait(1)
        sim.setJointTargetVelocity(motorLeft,vL)
        sim.setJointTargetVelocity(motorRight,vR)
    
    pass

# See the user manual or the available code snippets for additional callback functions and details
